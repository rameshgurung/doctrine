<?php
namespace DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use models\Common\Currency;
use models\Common\Country;
use models\Common\State;
use models\Common\City;

class CountryFixture extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager){
		echo "\nAdding Currency...\n";

		$currency = new Currency;
		$currency->setName('Nepali Rupee');
		$currency->setIsoCode('NPR');
		// $currency->setCountry(NULL);
		$currency->setSymbol('NPR');
		$manager->persist($currency);
		$manager->flush();

		echo "\nCurrency added successfully\n";

		echo "\nAdding Country...\n";

		$fileContents = file(__DIR__.'/countries.txt');		
		foreach ($fileContents as $file) {
			$c = explode(";", $file);
			$country = new Country;			
			$country->setName($c['2']);				
			$country->setIso_2($c['3']);
			$country->setIso_3($c['4']);
			$dialingCode = str_replace(array('\n','\t','\n\n','\n\n'),'', $c['6']);		
			$country->setDialingCode(trim($dialingCode));			
			$manager->persist($country);
		}
		$manager->flush();

		echo "\nCountries added successfully\n";

		echo "\nAdding States...\n";

		$fileContents = file(__DIR__.'/states.txt');
		foreach ($fileContents as $file) {				
			$c = explode(";", $file);
			$state = new State;
			$country = $manager->getRepository('\models\Common\Country')->find($c['1']);
			$state->setCountry($country);
			$name = str_replace(array('\n','\t','\n\n','\n\n'),'', $c['2']);
			$state->setName(trim($name));
			$manager->persist($state);
		}
		$manager->flush();

		echo "\nStates added successfully\n";

		echo "\nAdding Cities...\n";

		$fileContents = file(__DIR__.'/cities.txt');
		foreach ($fileContents as $file) {
			$c = explode(";", $file);
			$city = new City;
			$state = $manager->getRepository('\models\Common\State')->find($c['1']);
			$city->setState($state);
			$name = str_replace(array('\n','\t','\n\n','\n\n'),'', $c['2']);
			$city->setName(trim($name));
			$manager->persist($city);
		}
		$manager->flush();

		echo "\nCities added successfully\n";
	}

	public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}