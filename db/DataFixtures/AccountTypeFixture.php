<?php
namespace DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use models\AccountType;

class AccountTypeFixture extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager){
		$accTypes = array(
			array("type"=> "REGISTRY_ADMIN", "allowSubType"=> "Y", "main"=> "Y", "displayName" => "Registry Admin"),
			array("type"=> "CLIENT", "allowSubType"=> "Y", "main"=> "Y", "displayName" => "Client"),
			array("type"=> "GOVERNMENT_ENTITY", "allowSubType"=> "Y", "main"=> "Y", "displayName" => "Govenrnment Entity"),
			array("type"=> "NON_CLIENT", "allowSubType"=> "N", "main"=> "N", "displayName" => "Non Client"),
			array("type"=> "SUB_USER", "allowSubType"=> "N", "main"=> "N", "displayName" => "Sub User"),
		);

		echo "\nAdding Account Types...\n";

		foreach ($accTypes as $acc) {
			$accountType = new AccountType;
			$accountType->setType($acc['type']);
			$accountType->setAllowSubType($acc['allowSubType']);
			$accountType->setMain($acc['main']);
			$accountType->setDisplayName($acc['displayName']);
			$manager->persist($accountType);
			if($acc['type']== 'REGISTRY_ADMIN'){
				$this->addReference('registryAccType', $accountType);
			}
		}
		$manager->flush();

		echo "Account Types added succesfully\n\n";
	}


	public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}
