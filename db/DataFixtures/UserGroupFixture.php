<?php
namespace DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use models\User\Group;

class UserGroupFixture extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager){
		echo "\nAdding User Groups...\n";
		$groups = array(
			array("name"=> "Super Admin", "desc"=> "Super Admin"),
			array("name"=> "Registry Admin", "desc"=> "Registry Admin", "subGroup"=>array(
				array("name"=>"General Users", "desc"=> "General Users"),
				array("name"=>"Security Users", "desc"=> "Security Users"),
			)),
			array("name"=> "Client", "desc"=> "Client", "subGroup"=>array(
				array("name"=>"General Users", "desc"=> "General Users"),
				array("name"=>"Security Users", "desc"=> "Security Users"),
			)),
			array("name"=> "Bankclient", "desc"=> "Bankclient", "subGroup"=>array(
				array("name"=>"General Users", "desc"=> "General Users"),
				array("name"=>"Security Users", "desc"=> "Security Users"),
				array("name"=>"Teller", "desc"=> "Teller"),
			)),
			array("name"=> "Government Entity", "desc"=> "Government Entity"),
		);

		foreach ($groups as $grp) {
			$group = new Group;
			$group->setName($grp['name']);
			$group->setDescription($grp['desc']);
			$manager->persist($group);

			if(array_key_exists("subGroup", $grp) && is_array($grp['subGroup']))
			{
				foreach ($grp['subGroup'] as $subGrp) {
					$subGroup = new Group;
					$subGroup->setName($subGrp['name']);
					$subGroup->setDescription($subGrp['desc']);
					$subGroup->setParentGroup($group);
					$manager->persist($subGroup);
				}
			}
			if($grp['name']=="Super Admin"){
				$this->addReference('superAdminGroup', $group);
			}
		}

		$manager->flush();

		echo "\nUser Groups added successfully\n";
	}

	public function getOrder()
    {
        return 3; // the order in which fixtures will be loaded
    }
}
?>