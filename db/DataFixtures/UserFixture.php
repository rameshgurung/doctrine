<?php
namespace DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use models\User;
use models\Ledger;

class UserFixture extends AbstractFixture implements OrderedFixtureInterface
{
	const ACCNUM = '1111111111';

	public function load(ObjectManager $manager){

		echo "\nAdding Super User...\n";
		echo "\nCreating Ledger...\n";

		$ledger = new Ledger;
		// $accountType = $manager->getRepository('models\AccountType')->findOneBy(array('type'=>'REGISTRY_ADMIN'));
		$accountType = $this->getReference('registryAccType');
		$ledger->setAccountType($accountType);
		$ledger->setIsUsed(TRUE);
		$ledger->setStatus(1);
		$ledger->setAccountNumber(self::ACCNUM);
		$ledger->setAccountHead('SUPER');
		$ledger->setBalance('0.00');
		$manager->persist($ledger);

		echo "\nLedger created successfully\n";

		$user = new User;
		$user->setUsername('superadmin');
		$user->setPassword('e10adc3949ba59abbe56e057f20f883e');
		$user->setFirstname('Super');
		$user->setLastname('Admin');
		$city = $manager->getRepository('models\Common\City')->findOneBy(array('name'=>'kathmandu'));
		// $group = $manager->getRepository('models\User\Group')->findOneBy(array('name'=>'Super Admin'));
		$group = $this->getReference('superAdminGroup');
		$user->setCity($city);
		$user->setPhone('1234567890');
		$user->setMobile('1234567890');
		$user->setEmail('asd@asd.asd');
		$user->setGroup($group);
		$user->setAccType($accountType);
		$user->setLedger($ledger);
		$user->setStatus(1);
		$user->setAccNumber(self::ACCNUM);
		$user->setApproved(1);
		$manager->persist($user);
		$manager->flush();

		echo "\nSuper User added successfully\n";
	}

	public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}
?>